package it.unibo.oop.lab.reactivegui03;

import it.unibo.oop.lab.reactivegui02.GuiWithExternalThread;

/**
 *
 * Notify after 10 seconds.
 *
 */
public class StopAgent extends Thread {
    private final GuiWithExternalThread gui;
    private static final int MILLISECONDS_TO_WAIT = 10000;
    /**
     * Send a signal after 10 seconds.
     * @param gui the target gui to which notify the event
     */
    public StopAgent(final GuiWithExternalThread gui) {
        this.gui = gui;
        this.start();
    }

    /**
     * Send the signal to stop.
     */
    @Override
    public void run() {
        try {
            Thread.sleep(MILLISECONDS_TO_WAIT);
            gui.eventNotified("STOP");
        } catch (final InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}

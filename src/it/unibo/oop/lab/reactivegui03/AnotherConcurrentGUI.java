package it.unibo.oop.lab.reactivegui03;

import it.unibo.oop.lab.reactivegui02.ConcurrentGui;

/**
 *
 * Exercise 3.
 *
 */
public class AnotherConcurrentGUI extends ConcurrentGui {
    private final StopAgent stopAgent = new StopAgent(this);

    /**
     *  Handles Event Notified.
     */
    @Override
    public void eventNotified(final String message) {
        if (message.equals("STOP")) {
            super.stopClicked();
        } else {
            super.eventNotified(message);
        }
    }
}

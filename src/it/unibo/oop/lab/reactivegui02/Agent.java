package it.unibo.oop.lab.reactivegui02;

/**
 *
 * The handler of the gui.
 *
 */
public class Agent extends Thread {
    private int counter;
    private int isIncreasing = 1;
    private boolean isAlive = true;
    private final GuiWithExternalThread gui;

    /**
     * Inizialize the timer.
     *
     * @param gui
     *            the gui to which signal changes
     */
    public Agent(final GuiWithExternalThread gui) {
        counter = 0;
        this.gui = gui;
        this.start();
    }

    /**
     * Set the runnable to decrease.
     */
    public void decrease() {
        isIncreasing = -1;
    }

    /**
     * Set the runnable to increase.
     */
    public void increase() {
        isIncreasing = 1;
    }

    /**
     * Stop the timer.
     */
    public void stopTimer() {
        isAlive = false;
    }

    /**
     * Return timer.
     *
     * @return the timer count.
     */
    public int getTimer() {
        return counter;
    }

    /**
     * Run the thread.
     */
    @Override
    public void run() {
        while (isAlive) {
            try {
                Thread.sleep(100);
                counter = counter + isIncreasing;
                gui.eventNotified(Integer.toString(counter));
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

package it.unibo.oop.lab.reactivegui02;

/**
 *
 * In order to make gui and thread communicate.
 *
 */
public interface GuiWithExternalThread {
    /**
     * In order to make gui and thread communicate.
     * @param message  the message from the agent.
     */
    void eventNotified(String message);
}

package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * A gui working with multithreading.
 *
 */
public class ConcurrentGui implements GuiWithExternalThread {
    private final JFrame frame = new JFrame();
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel text = new JLabel("0");
    private final JButton up = new JButton("Up");
    private final JButton down = new JButton("Down");
    private final JButton stop = new JButton("Stop");

    private final Agent timer = new Agent(this);

    /**
     * Builds the gui.
     */
    public ConcurrentGui() {
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        final JPanel canvas = new JPanel(new FlowLayout());
        canvas.add(text);
        canvas.add(up);
        canvas.add(down);
        canvas.add(stop);
        frame.add(canvas);
        stop.addActionListener(e -> stopClicked());
        up.addActionListener(e -> timer.increase());
        down.addActionListener(e -> timer.decrease());
    }

    /**
     * displays the gui.
     */
    public void display() {
        frame.setVisible(true);
    }

    /**
     * Handles the stop.
     */
    protected final void stopClicked() {
        timer.stopTimer();
        stop.setEnabled(false);
        up.setEnabled(false);
        down.setEnabled(false);
    }
    /**
     * Handles event.
     */
    @Override
    public void eventNotified(final String message) {
        SwingUtilities.invokeLater(() -> text.setText(message));
    }
}

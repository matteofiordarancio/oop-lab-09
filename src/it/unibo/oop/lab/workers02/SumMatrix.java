package it.unibo.oop.lab.workers02;

/**
 *
 * An interfaced defining a sum for a matrix.
 *
 */
public interface SumMatrix {

    /**
     * @param matrix
     *            an arbitrary-sized matrix
     * @return the sum of its elements
     * @throws InterruptedException if something goes wrong.
     */
    double sum(double[][] matrix) throws InterruptedException;

}

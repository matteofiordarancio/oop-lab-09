package it.unibo.oop.lab.workers02;

/**
 *
 * Implementation of SumMatrix.
 *
 */
public class SumMatrixImpl implements SumMatrix {
    private final int nThread;
    private volatile double sumCounter;

    /**
     * Calculate the sum for a Matrix.
     *
     * @param nThread
     *            the number of thread to use
     */
    public SumMatrixImpl(final int nThread) {
        this.nThread = nThread;
    }

    /**
     * Calculates total sum for a matrix.
     */
    @Override
    public double sum(final double[][] matrix) throws InterruptedException {
        sumCounter = SumArray.getSum(matrix[0]);
        final Thread[] possibleThread = new Thread[nThread];
        final double[] partialSum = new double[nThread];
        final boolean[] hasJoined = new boolean[nThread];
        for (int i = 1; i < matrix.length; i++) {
            final int threadIndex = i;
            possibleThread[i % nThread] = new Thread(() -> {
                partialSum[threadIndex % nThread] = SumArray.getSum(matrix[threadIndex]);
                hasJoined[threadIndex % nThread] = false;
            });
            possibleThread[i % nThread].start();
            System.out.println(i);
            if (i % nThread == 0) {
                for (int j = 0; j < possibleThread.length; j++) {
                    possibleThread[j].join();
                    hasJoined[j] = true;
                    sumCounter += partialSum[j];
                }
            }
        }
        for (int j = 0; j < possibleThread.length; j++) {
            if (!hasJoined[j]) {
                possibleThread[j].join();
                sumCounter += partialSum[j];
            }
        }
        System.out.println(sumCounter + "  " + nThread);
        return sumCounter;
    }


    private static final class SumArray {
        /**
         * Implements a banal sum for an array.
         *
         * @param array
         *            b.
         */
        public static double getSum(final double[] array) {
            double sum = 0;
            for (int i = 0; i < array.length; i++) {
                sum += array[i];
            }
            return sum;
        }
    }

}
